module.exports = {
  clientId: "<client_id>",
  clientSecrect: "<client_secret>",
  tokenURL: "https://connect.pointclickcare.com/auth/token",
  revokeURL: "https://connect.pointclickcare.com/auth/revoke",
  loginURL: "https://connect.pointclickcare.com/auth/login",
  resourceURL: "https://connect.pointclickcare.com/api/public/preview1/orgs/",
  hostURL: "localhost"
};
